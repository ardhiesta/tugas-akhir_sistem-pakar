-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: spakardb
-- ------------------------------------------------------
-- Server version	5.5.37-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gejala`
--

DROP TABLE IF EXISTS `gejala`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gejala` (
  `IDGejala` varchar(4) NOT NULL,
  `DescGejala` varchar(200) NOT NULL,
  PRIMARY KEY (`IDGejala`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gejala`
--

LOCK TABLES `gejala` WRITE;
/*!40000 ALTER TABLE `gejala` DISABLE KEYS */;
INSERT INTO `gejala` VALUES ('G01','Ada penarikan dinding dada ke dalam'),('G02','Adanya paralisis pusat pernapasan lengkap'),('G03','Adanya plak pada hasil pemeriksaan X-ray paru'),('G04','Anoreksia'),('G05','Batuk'),('G06','Batuk pada malam hari atau cuaca dingin'),('G07','Batuk kronis'),('G08','Batuk berdahak lebih dari dua minggu'),('G09','Bau napas buruk'),('G10','Berat badan turun'),('G11','Berkeringat'),('G12','Berkurangnya daya penciuman'),('G13','Berkurangnya daya pengecap'),('G14','Bersin berulang-ulang (> 5x) terutama pada pagi hari atau kontak dengan debu'),('G15','Cairan di rongga pleura'),('G16','Darah dalam dahak'),('G17','Demam'),('G18','Demam tidak terlalu tinggi pada malam hari'),('G19','Denyut jantung beberapa saat masih ada, lalu napas berhenti'),('G20','Denyut jantung lambat'),('G21','Depresi pusat pernapasan (napas lemah)'),('G22','Diare'),('G23','Hidung tersumbat'),('G24','Kaku dan sakit pada otot leher'),('G25','Kejang'),('G26','Kejang klonik'),('G27','Kejang tonik'),('G28','Kelainan kuku (clubbing of fingers)'),('G29','Kelenjar limfa di sudut rahang bawah teraba dan nyeri'),('G30','Keluar ingus yang encer dan banyak'),('G31','Keringat malam'),('G32','Kesadaran mulai hilang'),('G33','Konstipasi'),('G34','Kulit lembab'),('G35','Kulit merah pada wajah serta kemerahan pada mulut, tenggorokan, dan hidung'),('G36','Lapisan tebal terbentuk menutupi belakang kerongkongan'),('G37','Lemas'),('G38','Leukosit tinggi'),('G39','Mata gatal dan kadang disertai dengan keluarnya air mata'),('G40','Mengantuk'),('G41','Mengeluarkan lendir dari mulut dan hidung'),('G42','Menghasilkan lendir yang menyumbat batang tenggorokan'),('G43','Mengi'),('G44','Merokok'),('G45','Mual'),('G46','Mukosa faring merah'),('G47','Muntah yang parah'),('G48','Nadi cepat'),('G49','Napas berat'),('G50','Napas cepat'),('G51','Nyeri abdomen'),('G52','Nyeri dada'),('G53','Nyeri dan merasa tertekan pada wajah'),('G54','Nyeri jika bernapas'),('G55','Nyeri kepala'),('G56','Nyeri otot'),('G57','Nyeri perut'),('G58','Nyeri tubuh, terutama sendi dan tenggorok'),('G59','Opistotonik'),('G60','Pemeriksaan tiga kali positif'),('G61','Penurunan nafsu makan'),('G62','Perasaan ditikam di dada'),('G63','Perasaan sering tiba-tiba dingin'),('G64','Pernapasan sesak'),('G65','Pinggir palatun molle agak hiperemis'),('G66','Polip tampak seperti air mata dan jika matang bentuknya menyerupai anggur'),('G67','Pupil dilatasi'),('G68','Pusing'),('G69','Rasa sesak di dada'),('G70','Sakit di punggung kaki dan tangan'),('G71','Sakit kepala'),('G72','Sakit tenggorokan'),('G73','Sesak napas'),('G74','Suara sengau'),('G75','Suara serak'),('G76','Suhu tubuh naik'),('G77','Sulit menelan'),('G78','Tangan atau kaki membiru'),('G79','Tekanan darah naik'),('G80','Tekanan darah turun'),('G81','Tinggal serumah dengan penderita TB'),('G82','Tonsil membesar');
/*!40000 ALTER TABLE `gejala` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penyakit`
--

DROP TABLE IF EXISTS `penyakit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penyakit` (
  `IDPenyakit` varchar(4) NOT NULL,
  `DescPenyakit` varchar(50) NOT NULL,
  `DefinisiPenyakit` text,
  PRIMARY KEY (`IDPenyakit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penyakit`
--

LOCK TABLES `penyakit` WRITE;
/*!40000 ALTER TABLE `penyakit` DISABLE KEYS */;
INSERT INTO `penyakit` VALUES ('P01','Polip','Polip hidung adalah masa polipoidal yang timbul terutama dari selaput lendir hidung dan sinus paranasal.'),('P02','Difteri','Penyumbatan pada rongga faring maupun laring oleh lendir yang dihasilkan oleh kuman difteri.'),('P03','Asidosis','Penyakit yang disebabkan oleh meningkatnya kadar asam karbonat dan asam bikarbonat dalam darah yang menyebabkan terganggunya pernapasan.'),('P04','Pleuritis','Radang pada pleura (selaput paru-paru).'),('P05','Bronkhitis','Gangguan pada cabang batang tenggorokan akibat infeksi.'),('P06','Ringitis','Radang pada hidung.'),('P07','Sinusitis','Peradangan pada rongga udara di dalam saluran hidung.'),('P08','Influenza (Flu)','Suatu infeksi virus yang menyebabkan demam, hidng meler, sakit kepala, batuk, tidak enak badan dan peradangan pada selaput lendir hidung dan saluran pernapasan.'),('P09','TB paru','Penyakit paru-paru yang disebabkan oleh mycobacterium tuberculosis. Bakteri tersebut menimbulkan bintil-bintil pada dinding alveolus. Jika Penyakit ini menyerang dan dibiarkan semakin luas, dapat menyebabkan sel-sel paru-paru mati. Akibatnya, paru-paru akan kuncup atau mengecil sehingga menyebabkan para penderita TBS napasnya sering terengah-engah.'),('P10','Suspek TB','Kemungkinan menderita TB paru. TB paru adalah penyakit paru-paru yang disebabkan oleh mycobacterium tuberculosis. Bakteri tersebut menimbulkan bintil-bintil pada dinding alveolus. Jika Penyakit ini menyerang dan dibiarkan semakin luas, dapat menyebabkan sel-sel paru-paru mati. Akibatnya, paru-paru akan kuncup atau mengecil sehingga menyebabkan para penderita TBS napasnya sering terengah-engah.'),('P11','Pneumonia','Penyakit infeksi yang disebabkan oleh virus atau bakteri patogen pada alveolus yang mengakibatkan radang paru-paru. Biasanya alveoli berisi cairan dan sel darah merah.'),('P12','Pneumonia Berat','Penyakit infeksi yang disebabkan oleh virus atau bakteri patogen pada alveolus yang mengakibatkan radang paru-paru. Biasanya alveoli berisi cairan dan sel darah merah.'),('P13','Infeksi Faring','Faringitis merupakan radang pada faring sehingga timbul rasa nyeri pada waktu menelan makanan.'),('P14','Suspek Infeksi Faring Akut','Kemungkinan menderita Infeksi Faring akut. Faringitis merupakan radang pada faring sehingga timbul rasa nyeri pada waktu menelan makanan.'),('P15','Asma','Kelainan penyumbatan saluran pernapasan yang disebabkan oleh alergi seperti debu, bulu, ataupun rambut. Kelainan ini dapat diturunkan dan juga dapat kambuh jika suhu lingkungan cukup rendah atau keadaan dingin.'),('P16','Asbestosis','Suatu penyakit saluran pernapasan yang terjadi akibat menghirup serat serat asbes dimana paru-paru terbentuk jaringan parut yang luas. Asbestos terdiri dari serat silikat mineral dengan komposisi kimiawi yang berbeda.'),('P17','Emfisema','Jenis penyakit paru obstruktif kronik yang melibatkan kerusakan pada kantung udara (alveoli) di paru-paru.'),('P18','Asfiksi','Gangguan dalam pengangkutan jaringan toksigen ke jaringan yang disebabkan oleh terganggunya fungsi paru-paru, pembuluh darah, atau jaringan tubuh.');
/*!40000 ALTER TABLE `penyakit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relasiGejalaRule`
--

DROP TABLE IF EXISTS `relasiGejalaRule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relasiGejalaRule` (
  `IDGejala` varchar(4) NOT NULL,
  `IDRule` varchar(4) NOT NULL,
  KEY `IDGejala` (`IDGejala`),
  KEY `IDRule` (`IDRule`),
  CONSTRAINT `relasiGejalaRule_ibfk_1` FOREIGN KEY (`IDGejala`) REFERENCES `gejala` (`IDGejala`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relasiGejalaRule_ibfk_2` FOREIGN KEY (`IDRule`) REFERENCES `rule` (`IDRule`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relasiGejalaRule`
--

LOCK TABLES `relasiGejalaRule` WRITE;
/*!40000 ALTER TABLE `relasiGejalaRule` DISABLE KEYS */;
INSERT INTO `relasiGejalaRule` VALUES ('G03','R01'),('G08','R01'),('G60','R01'),('G44','R02'),('G81','R02'),('G08','R03'),('G18','R03'),('G31','R03'),('G61','R03'),('G10','R03'),('G37','R03'),('G43','R04'),('G15','R04'),('G52','R04'),('G50','R05'),('G64','R05'),('G01','R05'),('G43','R05'),('G64','R06'),('G01','R06'),('G50','R06'),('G43','R06'),('G77','R06'),('G64','R07'),('G25','R07'),('G50','R07'),('G01','R07'),('G43','R07'),('G77','R07'),('G37','R08'),('G04','R08'),('G76','R08'),('G77','R08'),('G24','R08'),('G75','R08'),('G46','R08'),('G82','R08'),('G64','R08'),('G29','R08'),('G38','R08'),('G63','R09'),('G17','R09'),('G37','R09'),('G71','R09'),('G70','R09'),('G33','R09'),('G04','R09'),('G25','R09'),('G43','R10'),('G50','R10'),('G69','R10'),('G06','R10'),('G64','R10'),('G03','R11'),('G52','R11'),('G15','R11'),('G37','R11'),('G43','R11'),('G75','R11'),('G05','R11'),('G16','R11'),('G64','R11'),('G69','R11'),('G28','R11'),('G64','R12'),('G43','R12'),('G69','R12'),('G07','R12'),('G61','R12'),('G10','R12'),('G37','R12'),('G50','R13'),('G49','R13'),('G64','R13'),('G48','R13'),('G79','R13'),('G26','R13'),('G27','R13'),('G59','R13'),('G32','R13'),('G67','R13'),('G20','R13'),('G80','R13'),('G21','R13'),('G02','R13'),('G19','R13'),('G57','R14'),('G45','R14'),('G47','R14'),('G63','R14'),('G37','R14'),('G56','R14'),('G50','R14'),('G64','R14'),('G11','R14'),('G34','R14'),('G09','R14'),('G78','R14'),('G40','R14'),('G68','R14'),('G52','R15'),('G54','R15'),('G64','R15'),('G62','R15'),('G17','R15'),('G42','R15'),('G73','R15'),('G14','R16'),('G30','R16'),('G23','R16'),('G39','R16'),('G53','R17'),('G23','R17'),('G42','R17'),('G12','R17'),('G13','R17'),('G09','R17'),('G05','R17'),('G66','R18'),('G23','R18'),('G12','R18'),('G74','R18'),('G72','R19'),('G17','R19'),('G64','R19'),('G77','R19'),('G41','R19'),('G37','R19'),('G29','R19'),('G36','R19'),('G17','R20'),('G05','R20'),('G23','R20'),('G58','R20'),('G37','R20'),('G55','R20'),('G39','R20'),('G35','R20'),('G22','R20'),('G51','R20'),('G13','R21'),('G12','R21'),('G68','R21');
/*!40000 ALTER TABLE `relasiGejalaRule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relasiPenyakitRule`
--

DROP TABLE IF EXISTS `relasiPenyakitRule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relasiPenyakitRule` (
  `IDPenyakit` varchar(4) NOT NULL,
  `IDRule` varchar(4) NOT NULL,
  KEY `IDPenyakit` (`IDPenyakit`),
  KEY `IDRule` (`IDRule`),
  CONSTRAINT `relasiPenyakitRule_ibfk_1` FOREIGN KEY (`IDPenyakit`) REFERENCES `penyakit` (`IDPenyakit`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relasiPenyakitRule_ibfk_2` FOREIGN KEY (`IDRule`) REFERENCES `rule` (`IDRule`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relasiPenyakitRule`
--

LOCK TABLES `relasiPenyakitRule` WRITE;
/*!40000 ALTER TABLE `relasiPenyakitRule` DISABLE KEYS */;
INSERT INTO `relasiPenyakitRule` VALUES ('P01','R21');
/*!40000 ALTER TABLE `relasiPenyakitRule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule`
--

DROP TABLE IF EXISTS `rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule` (
  `IDRule` varchar(4) NOT NULL,
  `IDPenyakit` varchar(4) NOT NULL,
  `CFRule` double(4,3) NOT NULL,
  PRIMARY KEY (`IDRule`),
  KEY `IDPenyakit` (`IDPenyakit`),
  CONSTRAINT `rule_ibfk_1` FOREIGN KEY (`IDPenyakit`) REFERENCES `penyakit` (`IDPenyakit`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule`
--

LOCK TABLES `rule` WRITE;
/*!40000 ALTER TABLE `rule` DISABLE KEYS */;
INSERT INTO `rule` VALUES ('R01','P09',0.800),('R02','P10',0.600),('R03','P10',0.850),('R04','P10',0.700),('R05','P11',0.750),('R06','P12',0.800),('R07','P12',0.900),('R08','P13',0.900),('R09','P14',0.800),('R10','P15',1.000),('R11','P16',0.900),('R12','P17',0.850),('R13','P18',1.000),('R14','P03',0.800),('R15','P05',0.700),('R16','P06',0.950),('R17','P07',0.800),('R18','P01',0.600),('R19','P02',0.800),('R20','P08',0.900),('R21','P07',0.800);
/*!40000 ALTER TABLE `rule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-18 13:20:01
