package pakar.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JMenuBar;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JScrollPane;

import java.awt.Insets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import pakar.db.CFHasil;
import pakar.db.Gejala;
import pakar.db.KoneksiDb;
import pakar.db.Penyakit;
import pakar.db.Rule;

import javax.swing.JTextArea;

public class FrameUtama extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	FrameGejala fGejala = new FrameGejala();
	FramePenyakit fPenyakit = new FramePenyakit();
	FrameRule fRule = new FrameRule(); 
	JTextArea txtDebug = new JTextArea();
	JTextArea txtKeterangan = new JTextArea();
	private JTable tblGejala;
	private JTextField txtCF;
	private JTable tblFakta;
	private DefaultTableModel modelTblGejala;
	private DefaultTableModel modelTblFakta;

	/**
	 * Create the frame.
	 */
	public FrameUtama() {
		setTitle("Sistem Pakar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		this.setMinimumSize(new Dimension(640, 480));
		this.setLocationRelativeTo(null);
		
		final JLabel lblGejala = new JLabel("-- gejala belum dipilih --");
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnF = new JMenu("File");
		menuBar.add(mnF);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnF.add(mntmExit);
		
		JMenu mnMaster = new JMenu("Master");
		menuBar.add(mnMaster);
		
		JMenuItem mntmRule = new JMenuItem("Gejala");
		mntmRule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fGejala.setVisible(true);
			}
		});
		
		JMenuItem mntmPenyakit = new JMenuItem("Penyakit");
		mntmPenyakit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fPenyakit.setVisible(true);
			}
		});
		mnMaster.add(mntmPenyakit);
		mnMaster.add(mntmRule);
		
		JMenuItem mntmRule_1 = new JMenuItem("Rule");
		mntmRule_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fRule.setVisible(true);
			}
		});
		mnMaster.add(mntmRule_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 60, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblInputCfGejala = new JLabel("Input CF Gejala");
		GridBagConstraints gbc_lblInputCfGejala = new GridBagConstraints();
		gbc_lblInputCfGejala.anchor = GridBagConstraints.WEST;
		gbc_lblInputCfGejala.insets = new Insets(0, 0, 5, 5);
		gbc_lblInputCfGejala.gridx = 0;
		gbc_lblInputCfGejala.gridy = 0;
		contentPane.add(lblInputCfGejala, gbc_lblInputCfGejala);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		tblGejala = new JTable();
		
		final ListSelectionModel tblGejalaSelectionModel = tblGejala.getSelectionModel();
		tblGejalaSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblGejalaSelectionModel.addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!tblGejalaSelectionModel.isSelectionEmpty()){
					lblGejala.setText(tblGejala.getValueAt(tblGejala.getSelectedRow(), 1).toString());
				}
			}
		});
		
		modelTblGejala = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"ID Gejala","Gejala"
				}
			);
		
		tblGejala.setModel(modelTblGejala);
		scrollPane.setViewportView(tblGejala);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 3;
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 1;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 35, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lbl1 = new JLabel("Gejala");
		GridBagConstraints gbc_lbl1 = new GridBagConstraints();
		gbc_lbl1.anchor = GridBagConstraints.WEST;
		gbc_lbl1.insets = new Insets(0, 0, 5, 5);
		gbc_lbl1.gridx = 0;
		gbc_lbl1.gridy = 0;
		panel.add(lbl1, gbc_lbl1);
		
		JLabel lblNewLabel = new JLabel(":");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		GridBagConstraints gbc_lblGejala = new GridBagConstraints();
		gbc_lblGejala.anchor = GridBagConstraints.WEST;
		gbc_lblGejala.gridwidth = 2;
		gbc_lblGejala.insets = new Insets(0, 0, 5, 0);
		gbc_lblGejala.gridx = 2;
		gbc_lblGejala.gridy = 0;
		panel.add(lblGejala, gbc_lblGejala);
		
		JLabel lblCf = new JLabel("CF");
		GridBagConstraints gbc_lblCf = new GridBagConstraints();
		gbc_lblCf.anchor = GridBagConstraints.WEST;
		gbc_lblCf.insets = new Insets(0, 0, 5, 5);
		gbc_lblCf.gridx = 0;
		gbc_lblCf.gridy = 1;
		panel.add(lblCf, gbc_lblCf);
		
		JLabel lblNewLabel_1 = new JLabel(":");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 1;
		panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		String[] cfTitles = new String[] {"definitely not (-1.0)", 
				"almost certainly not (-0.8)", "probably not (-0.6)", "maybe not (-0.4)",
				"maybe (0.4)", "probably (0.6)", "almost certainly (0.8)", "definitely (1.0)", "others (user defined)"};
		final JComboBox comboCF = new JComboBox(cfTitles);
		GridBagConstraints gbc_comboCF = new GridBagConstraints();
		gbc_comboCF.gridwidth = 2;
		gbc_comboCF.insets = new Insets(0, 0, 5, 0);
		gbc_comboCF.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboCF.gridx = 2;
		gbc_comboCF.gridy = 1;
		panel.add(comboCF, gbc_comboCF);
		comboCF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int comboCFIndex = comboCF.getSelectedIndex();
				switch(comboCFIndex){
				case 0:
					txtCF.setText("-1.0");
					txtCF.setEditable(false);
					break;
				case 1:
					txtCF.setText("-0.8");
					txtCF.setEditable(false);
					break;
				case 2:
					txtCF.setText("-0.6");
					txtCF.setEditable(false);
					break;
				case 3:
					txtCF.setText("-0.4");
					txtCF.setEditable(false);
					break;
				case 4:
					txtCF.setText("0.4");
					txtCF.setEditable(false);
					break;
				case 5:
					txtCF.setText("0.6");
					txtCF.setEditable(false);
					break;
				case 6:
					txtCF.setText("0.8");
					txtCF.setEditable(false);
					break;
				case 7:
					txtCF.setText("1.0");
					txtCF.setEditable(false);
					break;
				case 8:
					txtCF.setText("");
					txtCF.setEditable(true);
					txtCF.requestFocus();
					break;
				}
			}
		});
		
		txtCF = new JTextField();
		//membatasi karakter yg bisa masuk
		txtCF.addKeyListener(new KeyAdapter() {
		    public void keyTyped(KeyEvent e) {
		    	 char karakter = e.getKeyChar();
		         if (!(Character.isDigit(karakter) || karakter==KeyEvent.VK_BACK_SPACE || karakter=='.')){
		             e.consume();
		         }
		      }
		    });
		txtCF.setText("-1.0");
		GridBagConstraints gbc_txtCF = new GridBagConstraints();
		gbc_txtCF.insets = new Insets(0, 0, 5, 5);
		gbc_txtCF.fill = GridBagConstraints.BOTH;
		gbc_txtCF.gridx = 2;
		gbc_txtCF.gridy = 2;
		panel.add(txtCF, gbc_txtCF);
		txtCF.setColumns(10);
		txtCF.setEditable(false);
		
		JButton btnTambah = new JButton("Tambah");
		btnTambah.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String idGejala = "";
				if(!tblGejalaSelectionModel.isSelectionEmpty()){
					idGejala = tblGejala.getValueAt(tblGejala.getSelectedRow(), 0).toString();
				}
				String cfValue = txtCF.getText();
				
				if(idGejala.trim().equals("") || cfValue.trim().equals("")){
					//noProcess
				} else {
					//proses
					//tidak boleh ada input dobel
					boolean adaGejalaDiTabel = false;
					for(int i=0; i<tblFakta.getRowCount(); i++){
						if(tblFakta.getValueAt(i, 0).toString().equals(idGejala)){
							modelTblFakta.removeRow(i);
//							tblFakta.remove(i);
//							modelTblFakta.fireTableDataChanged();
//							adaGejalaDiTabel = true;
							break;
						}
					}
					addTblFaktaRow(idGejala, tblGejala.getValueAt(tblGejala.getSelectedRow(), 1).toString(), cfValue);
					lblGejala.setText("-- gejala belum dipilih --");
					
//					if(!adaGejalaDiTabel){
//						addTblFaktaRow(idGejala, tblGejala.getValueAt(tblGejala.getSelectedRow(), 1).toString(), cfValue);
//						lblGejala.setText("-- gejala belum dipilih --");
//					} else {
//						JOptionPane.showMessageDialog(null, "Gejala yang dipilih sudah ditambahkan sebelumnya!", "Error", JOptionPane.ERROR_MESSAGE);
//					}
				}
			}
		});
		GridBagConstraints gbc_btnTambah = new GridBagConstraints();
		gbc_btnTambah.fill = GridBagConstraints.VERTICAL;
		gbc_btnTambah.insets = new Insets(0, 0, 5, 0);
		gbc_btnTambah.gridx = 3;
		gbc_btnTambah.gridy = 2;
		panel.add(btnTambah, gbc_btnTambah);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.gridwidth = 4;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 3;
		panel.add(scrollPane_1, gbc_scrollPane_1);
		
		tblFakta = new JTable();
		modelTblFakta = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"ID Gejala", "Gejala", "CF"
				}
			); 
		tblFakta.setModel(modelTblFakta);
		scrollPane_1.setViewportView(tblFakta);
		
		JButton btnClear = new JButton("Clear");
		GridBagConstraints gbc_btnClear = new GridBagConstraints();
		gbc_btnClear.anchor = GridBagConstraints.WEST;
		gbc_btnClear.insets = new Insets(0, 0, 5, 5);
		gbc_btnClear.gridx = 0;
		gbc_btnClear.gridy = 4;
		panel.add(btnClear, gbc_btnClear);
		
		JButton btnProses = new JButton("Proses");
		GridBagConstraints gbc_btnProses = new GridBagConstraints();
		gbc_btnProses.anchor = GridBagConstraints.EAST;
		gbc_btnProses.insets = new Insets(0, 0, 5, 0);
		gbc_btnProses.gridx = 3;
		gbc_btnProses.gridy = 4;
		panel.add(btnProses, gbc_btnProses);
		int intPercent = 0;
		
		JLabel lblHasil = new JLabel("Hasil :");
		GridBagConstraints gbc_lblHasil = new GridBagConstraints();
		gbc_lblHasil.insets = new Insets(0, 0, 5, 0);
		gbc_lblHasil.gridx = 3;
		gbc_lblHasil.gridy = 5;
		panel.add(lblHasil, gbc_lblHasil);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
		gbc_scrollPane_3.gridwidth = 4;
		gbc_scrollPane_3.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_3.gridx = 0;
		gbc_scrollPane_3.gridy = 6;
		panel.add(scrollPane_3, gbc_scrollPane_3);
		txtKeterangan.setEditable(false);
		txtKeterangan.setLineWrap(true);
		
		scrollPane_3.setViewportView(txtKeterangan);
		btnProses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int jmlFakta = tblFakta.getRowCount();
				if(jmlFakta > 0){
					txtDebug.setText("");
					//get fakta
					ArrayList<Gejala> arrGejalaFakta = new ArrayList<Gejala>();
					for(int i=0; i<jmlFakta; i++){
						Gejala gejala = new Gejala();
						gejala.setIdGejala(tblFakta.getValueAt(i, 0).toString());
						gejala.setDeskripsi(tblFakta.getValueAt(i, 1).toString());
						gejala.setCfValue(Double.parseDouble(tblFakta.getValueAt(i, 2).toString()));
						arrGejalaFakta.add(gejala);
					}
					//get rule
					getRule(arrGejalaFakta);
				}
			}
		});
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modelTblFakta.getDataVector().removeAllElements();
				modelTblFakta.fireTableDataChanged();
				txtDebug.setText("");
//				lbl_HasilFinal.setText("-- data belum diproses --");
				txtKeterangan.setText("");
			}
		});
		
		JButton btnRefresh = new JButton("Refresh Data Gejala");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadGejala();
			}
		});
		GridBagConstraints gbc_btnRefresh = new GridBagConstraints();
		gbc_btnRefresh.insets = new Insets(0, 0, 5, 5);
		gbc_btnRefresh.gridx = 0;
		gbc_btnRefresh.gridy = 2;
		contentPane.add(btnRefresh, gbc_btnRefresh);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 3;
		contentPane.add(scrollPane_2, gbc_scrollPane_2);
		txtDebug.setEditable(false);
		txtDebug.setLineWrap(true);
		
		scrollPane_2.setViewportView(txtDebug);
		
		loadGejala();
	}
	
	public void addTblFaktaRow(String idGejala, String deskripsi, String cfValue){
		modelTblFakta.fireTableDataChanged();
		Object[] o = new Object[5];
		o[0] = idGejala;
		o[1] = deskripsi;
		o[2] = cfValue;
		modelTblFakta.addRow(o);
		tblGejala.clearSelection();
	}
	
	public void loadGejala(){
		// menghapus seluruh data
		modelTblGejala.getDataVector().removeAllElements();
		// memberi tahu bahwa data telah kosong
		modelTblGejala.fireTableDataChanged();
		
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT * FROM gejala";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
			// lakukan penelusuran baris
				Object[] o = new Object[5];
				o[0] = r.getString("IDGejala");
				o[1] = r.getString("DescGejala");
				modelTblGejala.addRow(o);
			}
			r.close();
			s.close();
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	//isi CF gejala dari fakta
	public Gejala getGejalaById(String id, ArrayList<Gejala> arrGejalaFakta){
		Gejala gejala = new Gejala();
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT * FROM gejala WHERE IDGejala='"+id+"'";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
				gejala.setIdGejala(r.getString("IDGejala"));
				gejala.setDeskripsi(r.getString("DescGejala"));
				gejala.setCfValue(0);
				for(int i=0; i<arrGejalaFakta.size(); i++){
					Gejala gejalaFakta = arrGejalaFakta.get(i);
					if(gejalaFakta.getIdGejala().equals(r.getString("IDGejala"))){
						gejala.setCfValue(gejalaFakta.getCfValue());
					}
				}
			}
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return gejala;
	}
	
	public Penyakit getPenyakitById(String id/*, ArrayList<Gejala> arrGejalaFakta*/){
		Penyakit penyakit = new Penyakit();
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT * FROM penyakit WHERE IDPenyakit='"+id+"'";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
				penyakit.setIdPenyakit(r.getString("IDPenyakit"));
				penyakit.setDeskripsi(r.getString("DescPenyakit"));
				penyakit.setDefinisi(r.getString("DefinisiPenyakit"));
				penyakit.setCfValue(0);
			}
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return penyakit;
	}
	
	private ArrayList<Penyakit> getPenyakitPremis(String id){
		ArrayList<Penyakit> arrPenyakit = new ArrayList<Penyakit>();
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT * FROM relasiPenyakitRule WHERE IDRule='"+id+"'";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
				arrPenyakit.add(getPenyakitById(r.getString("IDPenyakit")));
			}
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return arrPenyakit;
	}
	
	private ArrayList<Gejala> getGejalaPremis(String id, ArrayList<Gejala> arrGejalaFakta){
		ArrayList<Gejala> arrGejala = new ArrayList<Gejala>();
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT * FROM relasiGejalaRule WHERE IDRule='"+id+"'";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
				arrGejala.add(getGejalaById(r.getString("IDGejala"), arrGejalaFakta));
			}
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return arrGejala;
	}
	
	public void getRule(ArrayList<Gejala> arrGejalaFakta){
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT ru.IDRule AS id_rule, "  
					+"p.IDPenyakit AS penyakit_konklusi, " 
					+"ru.CFRule AS cf_rule FROM rule ru "  
					+"LEFT JOIN penyakit p ON ru.IDPenyakit = p.IDPenyakit "  
					+"GROUP BY ru.IDRule;";
			ResultSet r = s.executeQuery(sql);
			ArrayList<Rule> arrRule = new ArrayList<Rule>();
			while(r.next()){
				Rule rule = new Rule();
				rule.setIdRule(r.getString("id_rule"));
				rule.setArrGejala(getGejalaPremis(r.getString("id_rule"), arrGejalaFakta));
				rule.setArrPenyakit(getPenyakitPremis(r.getString("id_rule")));
				rule.setPenyakit(getPenyakitById(r.getString("penyakit_konklusi")));
				rule.setCfValue(Double.parseDouble(r.getString("cf_rule")));
				
				arrRule.add(rule);
			}
			
			//debug1
			for(int i=0; i<arrRule.size(); i++){
				txtDebug.append("\n");
				txtDebug.append("--> Rule : "+arrRule.get(i).getIdRule()+"\n");
				txtDebug.append("--> Premis\n");
				if(arrRule.get(i).getArrGejala() != null){
					for(int j=0; j<arrRule.get(i).getArrGejala().size(); j++){
						txtDebug.append("ID Gejala : "+arrRule.get(i).getArrGejala().get(j).getIdGejala()+" ~ ");
						txtDebug.append("CF : "+arrRule.get(i).getArrGejala().get(j).getCfValue()+"\n");
					}
				}
//				txtDebug.append("-----------------------------\n");
				if(arrRule.get(i).getArrPenyakit() != null){
					for(int j=0; j<arrRule.get(i).getArrPenyakit().size(); j++){
						txtDebug.append("ID Penyakit : "+arrRule.get(i).getArrPenyakit().get(j).getIdPenyakit()+" ~ ");
						txtDebug.append("CF : "+arrRule.get(i).getArrPenyakit().get(j).getCfValue()+"\n");
					}
				}
				txtDebug.append("-----------------------------\n");
				txtDebug.append("--> Kesimpulan \n");
				txtDebug.append("ID Penyakit : "+arrRule.get(i).getPenyakit().getIdPenyakit()+" ~ ");
				txtDebug.append("CF Rule : "+arrRule.get(i).getCfValue()+"\n");
				txtDebug.append(">>>-----------------------<<<\n");
			}
			
			prosesHitungCF(arrRule);
			
			r.close();
			s.close();
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void prosesHitungCF(ArrayList<Rule> arrRule){
		//simpan ke ArrayList<CFHasil>
		ArrayList<CFHasil> arrCFHasil = new ArrayList<CFHasil>();
		for(int i=0; i<arrRule.size(); i++){
			CFHasil cfHasil = new CFHasil();
			cfHasil.setIdRule(arrRule.get(i).getIdRule());
			ArrayList<Gejala> arrG = arrRule.get(i).getArrGejala();
			
			double minCFGejala = 0;
			double minCFPenyakit = 0;
			if(arrG != null){
				List<Double> lCFGejala = new ArrayList<Double>();
				for(int j=0; j<arrG.size(); j++){
					lCFGejala.add(arrG.get(j).getCfValue());
				}
				if(lCFGejala.size() > 0){
					minCFGejala = Collections.min(lCFGejala);
				}
			}
			
			ArrayList<Penyakit> arrP = arrRule.get(i).getArrPenyakit();
			List<Double> lCFPenyakit = new ArrayList<Double>();
			if(arrP != null){
				for(int j=0; j<arrP.size(); j++){
					lCFPenyakit.add(arrP.get(j).getCfValue());
				}
				if(lCFPenyakit.size() > 0){
					minCFPenyakit = Collections.min(lCFPenyakit);
				}
			}
			
			if((arrG.size() > 0) && (arrP.size() > 0)){
				
			} else {
				if(arrG.size() > 0){
					cfHasil.setCfValue(minCFGejala*arrRule.get(i).getCfValue());
				} else if(arrP.size() > 0) {
					cfHasil.setCfValue(minCFPenyakit*arrRule.get(i).getCfValue());
				}
			}
			
			cfHasil.setIdPenyakit(arrRule.get(i).getPenyakit().getIdPenyakit());
			arrCFHasil.add(cfHasil);
		}
		
		//debug2
		txtDebug.append("\n");
		txtDebug.append("--> DEBUG 2 : CF untuk masing-masing penyakit yang Rule-nya tereksekusi\n");
		for(int i=0; i<arrCFHasil.size(); i++){
			txtDebug.append("\n");
			txtDebug.append("ID Rule : "+arrCFHasil.get(i).getIdRule()+" ~ ");
			txtDebug.append("ID Penyakit : "+arrCFHasil.get(i).getIdPenyakit()+" ~ ");
			txtDebug.append("CF : "+arrCFHasil.get(i).getCfValue()+"\n");
			txtDebug.append(">>>-----------------------<<<\n");
		}
		
		ArrayList<Rule> arrRuleTemp = new ArrayList<Rule>();
		ArrayList<CFHasil> arrCFHasilTemp = new ArrayList<CFHasil>();
		//isi CF penyakit yg jd premis
		for(int i=0; i<arrRule.size(); i++){
			Rule ruleTemp = new Rule();
			ruleTemp.setIdRule(arrRule.get(i).getIdRule());
			CFHasil cfHasilTemp = new CFHasil();
			cfHasilTemp.setIdRule(arrRule.get(i).getIdRule());
			
			ArrayList<Gejala> arrG = arrRule.get(i).getArrGejala();
			double minCFGejala = 0;
			double minCFPenyakit = 0;
			if(arrG != null){
				List<Double> lCFGejala = new ArrayList<Double>();
				for(int j=0; j<arrG.size(); j++){
					lCFGejala.add(arrG.get(j).getCfValue());
				}
				if(lCFGejala.size() > 0){
					minCFGejala = Collections.min(lCFGejala);
				}
			}
			ruleTemp.setArrGejala(arrG);
			
			ArrayList<Penyakit> arrP = arrRule.get(i).getArrPenyakit();
			ArrayList<Penyakit> arrPTemp = new ArrayList<Penyakit>(); 
			if(arrP != null){
				List<Double> lCFPenyakit = new ArrayList<Double>();
				for(int j=0; j<arrP.size(); j++){
					Penyakit penyakit = new Penyakit();
					penyakit.setIdPenyakit(arrP.get(j).getIdPenyakit());
					penyakit.setDeskripsi(arrP.get(j).getDeskripsi());
					for(int k=0; k<arrCFHasil.size(); k++){
						if(arrCFHasil.get(k).getIdPenyakit().equals(arrP.get(j).getIdPenyakit())){
							lCFPenyakit.add(arrCFHasil.get(k).getCfValue());
							penyakit.setCfValue(arrCFHasil.get(k).getCfValue());
						}
					}
					arrPTemp.add(penyakit);
				}
				if(lCFPenyakit.size() > 0){
					minCFPenyakit = Collections.min(lCFPenyakit);
				}
			}
			ruleTemp.setArrPenyakit(arrPTemp);
			
			if((arrG.size() > 0) && (arrP.size() > 0)){
				//ada penyakit yg jd premis
				double minValue = Math.min(minCFGejala, minCFPenyakit);
				cfHasilTemp.setCfValue(minValue*arrRule.get(i).getCfValue());
			} else {
				if(arrG.size() > 0){
					cfHasilTemp.setCfValue(minCFGejala*arrRule.get(i).getCfValue());
				} else if(arrP.size() > 0) {
					cfHasilTemp.setCfValue(minCFPenyakit*arrRule.get(i).getCfValue());
				}
			}
			ruleTemp.setPenyakit(arrRule.get(i).getPenyakit());
			ruleTemp.setCfValue(arrRule.get(i).getCfValue());
			
			cfHasilTemp.setIdPenyakit(arrRule.get(i).getPenyakit().getIdPenyakit());
			
			arrRuleTemp.add(ruleTemp);
			arrCFHasilTemp.add(cfHasilTemp);
		}
		
		arrRule = arrRuleTemp;
		arrCFHasil = arrCFHasilTemp;
		
		//debug3
		txtDebug.append("\n");
		txtDebug.append("--> DEBUG 3 : mengisi CF penyakit yang menjadi premis\n");
		for(int i=0; i<arrRule.size(); i++){
			txtDebug.append("\n");
			txtDebug.append("--> Rule : "+arrRule.get(i).getIdRule()+"\n");
			txtDebug.append("--> Premis\n");
			if(arrRule.get(i).getArrGejala() != null){
				for(int j=0; j<arrRule.get(i).getArrGejala().size(); j++){
					txtDebug.append("ID Gejala : "+arrRule.get(i).getArrGejala().get(j).getIdGejala()+" ~ ");
					txtDebug.append("CF : "+arrRule.get(i).getArrGejala().get(j).getCfValue()+"\n");
				}
			}
//			txtDebug.append("-----------------------------\n");
			if(arrRule.get(i).getArrPenyakit() != null){
				for(int j=0; j<arrRule.get(i).getArrPenyakit().size(); j++){
					txtDebug.append("ID Penyakit : "+arrRule.get(i).getArrPenyakit().get(j).getIdPenyakit()+" ~ ");
					txtDebug.append("CF : "+arrRule.get(i).getArrPenyakit().get(j).getCfValue()+"\n");
				}
			}
			txtDebug.append("-----------------------------\n");
			txtDebug.append("--> Kesimpulan \n");
			txtDebug.append("ID Penyakit : "+arrRule.get(i).getPenyakit().getIdPenyakit()+" ~ ");
			txtDebug.append("CF Rule : "+arrRule.get(i).getCfValue()+"\n");
			txtDebug.append(">>>-----------------------<<<\n");
		}
		
//		for(int i=0; i<arrRule.size(); i++){
//				txtDebug.append("\n");
//				txtDebug.append(arrRule.get(i).getIdRule()+"\n");
//				txtDebug.append("-----------------------------\n");
//				if(arrRule.get(i).getArrGejala() != null){
//					for(int j=0; j<arrRule.get(i).getArrGejala().size(); j++){
//						txtDebug.append(arrRule.get(i).getArrGejala().get(j).getIdGejala()+" ~ ");
//						txtDebug.append(arrRule.get(i).getArrGejala().get(j).getCfValue()+"\n");
//					}
//				}
//				txtDebug.append("-----------------------------\n");
//				if(arrRule.get(i).getArrPenyakit() != null){
//					for(int j=0; j<arrRule.get(i).getArrPenyakit().size(); j++){
//						txtDebug.append(arrRule.get(i).getArrPenyakit().get(j).getIdPenyakit()+" ~ ");
//						txtDebug.append(arrRule.get(i).getArrPenyakit().get(j).getCfValue()+"\n");
//					}
//				}
//				txtDebug.append("-----------------------------\n");
//				txtDebug.append(arrRule.get(i).getPenyakit().getIdPenyakit()+"\n");
//				txtDebug.append(arrRule.get(i).getCfValue()+"\n");
//				txtDebug.append(">>>--------------------------\n");
//		}
		
//		for(int j=0; j<arrRule.size(); j++){
//			txtDebug.append("--------------------------\n");
//			txtDebug.append(arrRule.get(j).getIdRule()+"\n");
//			
//			if(arrRule.get(j).getArrGejala() != null){
//				for(int k=0; k<arrRule.get(j).getArrGejala().size(); k++){
//					txtDebug.append(arrRule.get(j).getArrGejala().get(k).getIdGejala()+" ~ "+
//							arrRule.get(j).getArrGejala().get(k).getDeskripsi()+" ~ "+
//							arrRule.get(j).getArrGejala().get(k).getCfValue()+"\n");
//				}
//			}
//			
//			if(arrRule.get(j).getArrPenyakit() != null){
//				for(int k=0; k<arrRule.get(j).getArrPenyakit().size(); k++){
//					txtDebug.append(arrRule.get(j).getArrPenyakit().get(k).getIdPenyakit()+" ~ "+
//							arrRule.get(j).getArrPenyakit().get(k).getDeskripsi()+" ~ "+
//							arrRule.get(j).getArrPenyakit().get(k).getCfValue()+"\n");
//				}
//			}
//			
//			txtDebug.append(arrRule.get(j).getPenyakit().getIdPenyakit()+" ~ "+arrRule.get(j).getPenyakit().getDeskripsi()+"\n");
//			txtDebug.append(arrRule.get(j).getCfValue()+"\n");
//			txtDebug.append("--------------------------\n");
//		}
		
////		for(int j=0; j<arrCFHasil.size(); j++){
////			txtDebug.append(">>---------------------->>\n");
////			txtDebug.append(arrCFHasil.get(j).getIdRule()+"\n");
////			txtDebug.append(arrCFHasil.get(j).getIdPenyakit()+"\n");
////			txtDebug.append(arrCFHasil.get(j).getCfValue()+"\n");
////			txtDebug.append(">>---------------------->>\n");
////		}
//		
		//TODO: here
		ArrayList<CFHasil> arrCFHasilFinal = new ArrayList<CFHasil>();
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT DISTINCT(IDPenyakit) FROM rule";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
				CFHasil cFHasilFinal = new CFHasil();
				cFHasilFinal.setIdPenyakit(r.getString("IDPenyakit"));
				double cfTemp = 0;
				int counter = 0;
				for(int i=0; i<arrCFHasil.size(); i++){
					if(arrCFHasil.get(i).getIdPenyakit().equals(r.getString("IDPenyakit"))){
//						System.out.println("idRule ~ "+arrCFHasil.get(i).getIdRule());
						if(counter == 0){
							cfTemp = arrCFHasil.get(i).getCfValue();
//							System.out.println("0 ~ "+cfTemp);
						} else {
							if(cfTemp > 0 && arrCFHasil.get(i).getCfValue() > 0){
								cfTemp = cfTemp + arrCFHasil.get(i).getCfValue() - (cfTemp * arrCFHasil.get(i).getCfValue());
							} else if(cfTemp < 0 && arrCFHasil.get(i).getCfValue() < 0){
								cfTemp = cfTemp + arrCFHasil.get(i).getCfValue() + (cfTemp * arrCFHasil.get(i).getCfValue());
							} else {
								List<Double> lCFHitung = new ArrayList<Double>();
								lCFHitung.add(Math.abs(cfTemp));
								lCFHitung.add(Math.abs(arrCFHasil.get(i).getCfValue()));
								double minimalCF = Collections.min(lCFHitung);
//								Math.abs(a);
								cfTemp = (cfTemp + arrCFHasil.get(i).getCfValue()) / (1 - minimalCF);
							}
							
						}
						counter++;
					}
				}
				NumberFormat formatter = new DecimalFormat("#0.000");
//				cFHasilFinal.setCfValue(cfTemp);
				cFHasilFinal.setCfValue(Double.parseDouble(formatter.format(cfTemp)));
				arrCFHasilFinal.add(cFHasilFinal);
			}
			r.close();
			s.close();
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
//		
////		Collections.sort(arrCFHasilFinal);
		Collections.sort(arrCFHasilFinal, new Comparator<CFHasil>() {
		    @Override
		    public int compare(CFHasil c1, CFHasil c2) {
		        return Double.compare(c2.getCfValue(), c1.getCfValue());
		    }
		});
		
		txtDebug.append("\n");
		txtDebug.append("--> FINAL\n");
		for(int i=0; i<arrCFHasilFinal.size(); i++){
			txtDebug.append("ID Penyakit : "+arrCFHasilFinal.get(i).getIdPenyakit()+" ~ ");
			txtDebug.append("CF Final : "+arrCFHasilFinal.get(i).getCfValue()+"\n");
		}
//		lbl_HasilFinal.setText(getPenyakitById(arrCFHasilFinal.get(0).getIdPenyakit()).getDeskripsi());
		txtKeterangan.setText("");
		if(arrCFHasilFinal.get(0).getCfValue() <= 0){
			txtKeterangan.append("unknown");
		} else {
			txtKeterangan.append(getPenyakitById(arrCFHasilFinal.get(0).getIdPenyakit()).getDeskripsi()+"\n"
					+getPenyakitById(arrCFHasilFinal.get(0).getIdPenyakit()).getDefinisi());
		}
	}
}

/*
 mysql> SELECT re.IDGejala, ru.IDRule FROM rule ru LEFT JOIN relasi re ON ru.IDRule = re.IDRule;
+----------+--------+
| IDGejala | IDRUle |
+----------+--------+
| G01      | R01    |
| G04      | R01    |
| G05      | R03    |
| G01      | R02    |
| G02      | R02    |
| NULL     | R04    |
| NULL     | R05    |
+----------+--------+

SELECT re.IDGejala, ru.IDRule, ru.CFRule FROM rule ru LEFT JOIN relasi re ON ru.IDRule = re.IDRule;
+----------+--------+--------+
| IDGejala | IDRule | CFRule |
+----------+--------+--------+
| G01      | R01    |    0.6 |
| G04      | R01    |    0.6 |
| G01      | R02    |    0.8 |
| G02      | R02    |    0.8 |
| G05      | R03    |    0.5 |
| NULL     | R04    |    0.8 |
| NULL     | R05    |      1 |
+----------+--------+--------+

SELECT re.IDGejala, p.IDPenyakit, ru.IDRule, ru.CFRule 
FROM rule ru 
LEFT JOIN relasi re ON ru.IDRule = re.IDRule 
LEFT JOIN penyakit p ON ru.IDPenyakit = p.IDPenyakit;
+----------+------------+--------+--------+
| IDGejala | IDPenyakit | IDRule | CFRule |
+----------+------------+--------+--------+
| G01      | P01        | R01    |    0.6 |
| G04      | P01        | R01    |    0.6 |
| G01      | P02        | R02    |    0.8 |
| G02      | P02        | R02    |    0.8 |
| G05      | P01        | R03    |    0.5 |
| NULL     | P03        | R04    |    0.8 |
| NULL     | P04        | R05    |      1 |
+----------+------------+--------+--------+

SELECT 
ru.IDRule AS id_rule, rgr.IDGejala AS gejala_premis, 
rpr.IDPenyakit AS penyakit_premis, p.IDPenyakit AS penyakit_konklusi, 
ru.CFRule AS cf_rule 
FROM rule ru 
LEFT JOIN relasiGejalaRule rgr ON ru.IDRule = rgr.IDRule 
LEFT JOIN penyakit p ON ru.IDPenyakit = p.IDPenyakit 
LEFT JOIN relasiPenyakitRule rpr ON ru.IDRule = rpr.IDRule;
+---------+---------------+-----------------+-------------------+---------+
| id_rule | gejala_premis | penyakit_premis | penyakit_konklusi | cf_rule |
+---------+---------------+-----------------+-------------------+---------+
| R01     | G01           | NULL            | P01               |     0.6 |
| R01     | G04           | NULL            | P01               |     0.6 |
| R02     | G01           | NULL            | P02               |     0.8 |
| R02     | G02           | NULL            | P02               |     0.8 |
| R03     | G05           | NULL            | P01               |     0.5 |
| R04     | G03           | P02             | P03               |     0.8 |
| R05     | G06           | P02             | P04               |       1 |
+---------+---------------+-----------------+-------------------+---------+

SELECT ru.IDRule AS id_rule, GROUP_CONCAT(rgr.IDGejala SEPARATOR ', ') AS gejala_penyakit, 
rpr.IDPenyakit AS penyakit_premis, p.IDPenyakit AS penyakit_konklusi, 
ru.CFRule AS cf_rule FROM rule ru 
LEFT JOIN relasiGejalaRule rgr ON ru.IDRule = rgr.IDRule 
LEFT JOIN penyakit p ON ru.IDPenyakit = p.IDPenyakit 
LEFT JOIN relasiPenyakitRule rpr ON ru.IDRule = rpr.IDRule GROUP BY ru.IDRule;
+---------+-----------------+-----------------+-------------------+---------+
| id_rule | gejala_penyakit | penyakit_premis | penyakit_konklusi | cf_rule |
+---------+-----------------+-----------------+-------------------+---------+
| R01     | G01, G04        | NULL            | P01               |     0.6 |
| R02     | G01, G02        | NULL            | P02               |     0.8 |
| R03     | G05             | NULL            | P01               |     0.5 |
| R04     | G03             | P02             | P03               |     0.8 |
| R05     | G06             | P02             | P04               |       1 |
+---------+-----------------+-----------------+-------------------+---------+

SELECT ru.IDRule AS id_rule, GROUP_CONCAT(rgr.IDGejala SEPARATOR ', ') AS gejala_premis, 
GROUP_CONCAT(rpr.IDPenyakit SEPARATOR ', ') AS penyakit_premis, 
p.IDPenyakit AS penyakit_konklusi, 
ru.CFRule AS cf_rule FROM rule ru 
LEFT JOIN relasiGejalaRule rgr ON ru.IDRule = rgr.IDRule 
LEFT JOIN penyakit p ON ru.IDPenyakit = p.IDPenyakit 
LEFT JOIN relasiPenyakitRule rpr ON ru.IDRule = rpr.IDRule 
GROUP BY ru.IDRule;

+---------+---------------+-----------------+-------------------+---------+
| id_rule | gejala_premis | penyakit_premis | penyakit_konklusi | cf_rule |
+---------+---------------+-----------------+-------------------+---------+
| R01     | G01, G04      | NULL            | P01               |     0.6 |
| R02     | G01, G02      | NULL            | P02               |     0.8 |
| R03     | G05           | NULL            | P01               |     0.5 |
| R04     | G03           | P02             | P03               |     0.8 |
| R05     | G06           | P02             | P04               |       1 |
+---------+---------------+-----------------+-------------------+---------+

select GROUP_CONCAT(IDRule SEPARATOR ', ') AS IDRule, IDPenyakit from rule group by IDPenyakit;
+----------+------------+
| IDRule   | IDPenyakit |
+----------+------------+
| R01, R03 | P01        |
| R02      | P02        |
| R04      | P03        |
| R05      | P04        |
+----------+------------+


idRule: R01
----
IDGejala: G01
DescGejala: hurting
CFGejala: 1.0
----
----
IDGejala: G04
DescGejala: fever
CFGejala: 0.4
----
minCFGejala: 0.4
CFRule: 0.6
Hasil CFRule: 0.4 * 0.6 = 0.24
penyakit: P01
CFRule: 0.6

idRule: R02
----
IDGejala: G01
DescGejala: hurting
CFGejala: 1.0
----
----
IDGejala: G02
DescGejala: swollen
CFGejala: 0.6
----
minCFGejala: 0.6
CFRule: 0.8
Hasil CFRule: 0.6 * 0.8 = 0.48
penyakit: P02
CFRule: 0.8

idRule: R03
----
IDGejala: G05
DescGejala: overload
CFGejala: 1.0
----
minCFGejala: 1.0
CFRule: 0.5
Hasil CFRule: 1.0 * 0.5 = 0.5
penyakit: P01
CFRule: 0.5

idRule: R04
----
IDGejala: G03
DescGejala: red
CFGejala: 0.1
----
----
IDPenyakit: P02
DescPenyakit: trauma
CFPenyakit: 0.0
----
minCFGejala: 0.1
minCFPenyakit: 0.0
penyakit: P03
CFRule: 0.8

idRule: R05
----
IDGejala: G06
DescGejala: move
CFGejala: 1.0
----
----
IDPenyakit: P02
DescPenyakit: trauma
CFPenyakit: 0.0
----
minCFGejala: 1.0
minCFPenyakit: 0.0
penyakit: P04
CFRule: 1.0
*/