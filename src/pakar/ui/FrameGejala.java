package pakar.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.GridBagLayout;
import javax.swing.JTable;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import pakar.db.KoneksiDb;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrameGejala extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtDesc;
	private DefaultTableModel model;
	private JTable tblGejala;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					FrameGejala frame = new FrameGejala();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public FrameGejala() {
		setTitle("Data Gejala");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 67, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{35, 35, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblIdGejala = new JLabel("ID Gejala");
		GridBagConstraints gbc_lblIdGejala = new GridBagConstraints();
		gbc_lblIdGejala.anchor = GridBagConstraints.EAST;
		gbc_lblIdGejala.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdGejala.gridx = 0;
		gbc_lblIdGejala.gridy = 0;
		panel.add(lblIdGejala, gbc_lblIdGejala);
		
		txtId = new JTextField();
		GridBagConstraints gbc_txtId = new GridBagConstraints();
		gbc_txtId.insets = new Insets(0, 0, 5, 0);
		gbc_txtId.fill = GridBagConstraints.BOTH;
		gbc_txtId.gridx = 1;
		gbc_txtId.gridy = 0;
		panel.add(txtId, gbc_txtId);
		txtId.setColumns(10);
		
		JLabel lblDeskripsi = new JLabel("Deskripsi");
		GridBagConstraints gbc_lblDeskripsi = new GridBagConstraints();
		gbc_lblDeskripsi.anchor = GridBagConstraints.EAST;
		gbc_lblDeskripsi.insets = new Insets(0, 0, 5, 5);
		gbc_lblDeskripsi.gridx = 0;
		gbc_lblDeskripsi.gridy = 1;
		panel.add(lblDeskripsi, gbc_lblDeskripsi);
		
		txtDesc = new JTextField();
		GridBagConstraints gbc_txtDesc = new GridBagConstraints();
		gbc_txtDesc.insets = new Insets(0, 0, 5, 0);
		gbc_txtDesc.fill = GridBagConstraints.BOTH;
		gbc_txtDesc.gridx = 1;
		gbc_txtDesc.gridy = 1;
		panel.add(txtDesc, gbc_txtDesc);
		txtDesc.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.anchor = GridBagConstraints.NORTH;
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnTambah = new JButton("Tambah");
		btnTambah.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = txtId.getText();
				String desc = txtDesc.getText();
				
				if(id.trim().equals("") || desc.trim().equals("")){
					JOptionPane.showMessageDialog (null, "Id Gejala dan Deskripsi harus diisi!", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					try{
						Connection c = KoneksiDb.getKoneksi();
						String sql = "INSERT INTO gejala VALUES (?, ?)";
						PreparedStatement p = c.prepareStatement(sql);
						p.setString(1, id);
						p.setString(2, desc);
						p.executeUpdate();
						p.close();
					}catch(SQLException e0){
//						e0.printStackTrace();
						JOptionPane.showMessageDialog(null, e0.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}finally{
						loadData();
						kosongkanField();
					}
				}
			}
		});
		GridBagConstraints gbc_btnTambah = new GridBagConstraints();
		gbc_btnTambah.insets = new Insets(0, 0, 0, 5);
		gbc_btnTambah.gridx = 1;
		gbc_btnTambah.gridy = 0;
		panel_1.add(btnTambah, gbc_btnTambah);
		
		tblGejala = new JTable();
		model = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID Gejala", "Deskripsi"
				});
		tblGejala.setModel(model);
		
		JButton btnUbah = new JButton("Ubah");
		btnUbah.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = tblGejala.getSelectedRow();
//				System.out.println("-->i: "+i);
				if(i == -1){
					// tidak ada baris terseleksi
					return;
				}
				
				// ambil yang terseleksi
				String prevId = (String) model.getValueAt(i, 0);
				String id = txtId.getText();
				String desc = txtDesc.getText();
//				System.out.println("-->i: "+prevId+", id: "+id+", desc: "+desc);
				
				if(id.trim().equals("") || desc.trim().equals("")){
					JOptionPane.showMessageDialog (null, "Id Gejala dan Deskripsi harus diisi!", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					try{
						Connection c = KoneksiDb.getKoneksi();
						String sql = "UPDATE gejala SET IDGejala = ?, DescGejala = ? WHERE IDGejala = ?";
						PreparedStatement p = c.prepareStatement(sql);
						p.setString(1, id);
						p.setString(2, desc);
						p.setString(3, prevId);
						p.executeUpdate();
						p.close();
					}catch(SQLException e1){
//						System.out.println("Terjadi Error");
						JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}finally{
						loadData();
						kosongkanField();
					}
				}
			}
		});
		GridBagConstraints gbc_btnUbah = new GridBagConstraints();
		gbc_btnUbah.insets = new Insets(0, 0, 0, 5);
		gbc_btnUbah.gridx = 2;
		gbc_btnUbah.gridy = 0;
		panel_1.add(btnUbah, gbc_btnUbah);
		
		JButton btnHapus = new JButton("Hapus");
		btnHapus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = tblGejala.getSelectedRow();
				if(i == -1){
					// tidak ada baris terseleksi
					return;
				}
				
				String id = (String) model.getValueAt(i, 0);
				
				int response = JOptionPane.showConfirmDialog(null, "Hapus gejala "+id+"?", "Hapus Gejala",
				        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				
				if (response == JOptionPane.YES_OPTION) {
					try{
						Connection c = KoneksiDb.getKoneksi();
						String sql = "DELETE FROM gejala WHERE IDGejala = ?";
						PreparedStatement p = c.prepareStatement(sql);
						p.setString(1, id);
						p.executeUpdate();
						p.close();
					} catch(SQLException e2){
//						System.err.println("Terjadi Error");
						JOptionPane.showMessageDialog (null, e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}finally{
						loadData();
						kosongkanField();
					}
				}
			}
		});
		GridBagConstraints gbc_btnHapus = new GridBagConstraints();
		gbc_btnHapus.gridx = 3;
		gbc_btnHapus.gridy = 0;
		panel_1.add(btnHapus, gbc_btnHapus);
		
//		tblGejala.setCellSelectionEnabled(true);
		final ListSelectionModel tblGejalaSelectionModel = tblGejala.getSelectionModel();
		tblGejalaSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tblGejalaSelectionModel.addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!tblGejalaSelectionModel.isSelectionEmpty()){
					txtId.setText(tblGejala.getValueAt(tblGejala.getSelectedRow(), 0).toString());
					txtDesc.setText(tblGejala.getValueAt(tblGejala.getSelectedRow(), 1).toString());
				}
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(tblGejala);
		
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 2;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		//ambil data gejala
		loadData();
	}

	public void loadData(){
		// menghapus seluruh data
		model.getDataVector().removeAllElements();
		// memberi tahu bahwa data telah kosong
		model.fireTableDataChanged();
		
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT * FROM gejala";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
			// lakukan penelusuran baris
				Object[] o = new Object[5];
				o[0] = r.getString("IDGejala");
				o[1] = r.getString("DescGejala");
				model.addRow(o);
			}
			r.close();
			s.close();
//			c.close();
		} catch (SQLException e){
//			System.out.println("Terjadi Error");
//			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void kosongkanField(){
		tblGejala.clearSelection();
		txtId.setText("");
		txtDesc.setText("");
	}
}
