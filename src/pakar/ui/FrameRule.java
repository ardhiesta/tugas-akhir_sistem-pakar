package pakar.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;

import pakar.db.Gejala;
import pakar.db.KoneksiDb;
import pakar.db.Rule;

public class FrameRule extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tblRule;
	private DefaultTableModel modelTblRule;

	/**
	 * Create the frame.
	 */
	public FrameRule() {
		setTitle("Rule");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		this.setMinimumSize(new Dimension(640, 480));
		
		JLabel lblRule = new JLabel("Rule :");
		GridBagConstraints gbc_lblRule = new GridBagConstraints();
		gbc_lblRule.insets = new Insets(0, 0, 5, 0);
		gbc_lblRule.gridx = 0;
		gbc_lblRule.gridy = 0;
		contentPane.add(lblRule, gbc_lblRule);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		modelTblRule = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"ID Rule","Rule","CF"
				}
			);
		tblRule = new JTable();
		tblRule.setModel(modelTblRule);
		scrollPane.setViewportView(tblRule);
		tblRule.getColumnModel().getColumn(0).setPreferredWidth(10);
		tblRule.getColumnModel().getColumn(2).setPreferredWidth(10);
		
		final ListSelectionModel tblRuleSelectionModel = tblRule.getSelectionModel();
		tblRuleSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblRuleSelectionModel.addListSelectionListener(new ListSelectionListener(){
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!tblRuleSelectionModel.isSelectionEmpty()){
//					lblGejala.setText(tblGejala.getValueAt(tblGejala.getSelectedRow(), 1).toString());
//					lblKonklusi.setText(getRuleById("").getPenyakit().getDeskripsi());
				}
			}
		});
		
		getRule();
	}
	
	//TODO : here
	private Rule getRuleById(String id){
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		return null;
	}
	
//	public Gejala getGejalaById(String id){
//		Gejala gejala = new Gejala();
//		try{
//			Connection c = KoneksiDb.getKoneksi();
//			Statement s = c.createStatement();
//			String sql = "SELECT * FROM gejala WHERE IDGejala='"+id+"'";
//			ResultSet r = s.executeQuery(sql);
//			while(r.next()){
//				gejala.setIdGejala(r.getString("IDGejala"));
//				gejala.setDeskripsi(r.getString("DescGejala"));
//				gejala.setCfValue(0);
////				for(int i=0; i<arrGejalaFakta.size(); i++){
////					Gejala gejalaFakta = arrGejalaFakta.get(i);
////					if(gejalaFakta.getIdGejala().equals(r.getString("IDGejala"))){
////						gejala.setCfValue(gejalaFakta.getCfValue());
////					}
////				}
//			}
//		} catch (SQLException e){
//			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
//		}
//		return gejala;
//	}
//	
//	private ArrayList<Gejala> getGejalaPremis(String id){
//		ArrayList<Gejala> arrGejala = new ArrayList<Gejala>();
//		try{
//			Connection c = KoneksiDb.getKoneksi();
//			Statement s = c.createStatement();
//			String sql = "SELECT * FROM relasiGejalaRule WHERE IDRule='"+id+"'";
//			ResultSet r = s.executeQuery(sql);
////			while(r.next()){
////				arrGejala.add(getGejalaById(r.getString("IDGejala"), arrGejalaFakta));
////			}
//		} catch (SQLException e){
//			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
//		}
//		return arrGejala;
//	}

	public void getRule(){
		try{
			Connection c = KoneksiDb.getKoneksi();
			Statement s = c.createStatement();
			String sql = "SELECT ru.IDRule AS id_rule, GROUP_CONCAT(rgr.IDGejala SEPARATOR ', ') AS id_gejala_premis, " 
					+"GROUP_CONCAT(g.DescGejala SEPARATOR ' AND ') AS gejala_premis, "
					+"GROUP_CONCAT(rpr.IDPenyakit SEPARATOR ', ') AS id_penyakit_premis, " 
					+"GROUP_CONCAT(p2.DescPenyakit SEPARATOR ' AND ') AS penyakit_premis, "
					+"p.DescPenyakit AS penyakit_konklusi, ru.CFRule AS cf_rule " 
					+"FROM rule ru " 
					+"LEFT JOIN relasiGejalaRule rgr ON ru.IDRule = rgr.IDRule " 
					+"LEFT JOIN penyakit p ON ru.IDPenyakit = p.IDPenyakit " 
					+"LEFT JOIN relasiPenyakitRule rpr ON ru.IDRule = rpr.IDRule " 
					+"LEFT JOIN gejala g ON rgr.IDGejala = g.IDGejala " 
					+"LEFT JOIN penyakit p2 ON rpr.IDpenyakit = p2.IDPenyakit " 
					+"GROUP BY ru.IDRule";
			ResultSet r = s.executeQuery(sql);
			while(r.next()){
				Object[] o = new Object[5];
				o[0] = r.getString("id_rule");
				if(r.getString("gejala_premis") != null && r.getString("penyakit_premis") != null){
					o[1] = "IF "+r.getString("gejala_premis")+" AND "+r.getString("penyakit_premis")+" THEN "+r.getString("penyakit_konklusi");
				} else {
					if(r.getString("gejala_premis") != null){
						o[1] = "IF "+r.getString("gejala_premis")+" THEN "+r.getString("penyakit_konklusi");
					} else {
						o[1] = "IF "+r.getString("penyakit_premis")+" THEN "+r.getString("penyakit_konklusi");
					}
				}
				o[2] = r.getString("cf_rule");
				modelTblRule.addRow(o);
			}
			r.close();
			s.close();
		} catch (SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
/*
SELECT ru.IDRule AS id_rule, GROUP_CONCAT(rgr.IDGejala SEPARATOR ', ') AS id_gejala_premis, 
GROUP_CONCAT(g.DescGejala SEPARATOR ' AND ') AS gejala_premis, 
GROUP_CONCAT(rpr.IDPenyakit SEPARATOR ', ') AS id_penyakit_premis, 
GROUP_CONCAT(p2.DescPenyakit SEPARATOR ' AND ') AS penyakit_premis, 
p.IDPenyakit AS penyakit_konklusi, ru.CFRule AS cf_rule 
FROM rule ru 
LEFT JOIN relasiGejalaRule rgr ON ru.IDRule = rgr.IDRule 
LEFT JOIN penyakit p ON ru.IDPenyakit = p.IDPenyakit 
LEFT JOIN relasiPenyakitRule rpr ON ru.IDRule = rpr.IDRule 
LEFT JOIN gejala g ON rgr.IDGejala = g.IDGejala 
LEFT JOIN penyakit p2 ON rpr.IDpenyakit = p2.IDPenyakit 
GROUP BY ru.IDRule;
+---------+------------------+---------------------+--------------------+-----------------+-------------------+---------+
| id_rule | id_gejala_premis | gejala_premis       | id_penyakit_premis | penyakit_premis | penyakit_konklusi | cf_rule |
+---------+------------------+---------------------+--------------------+-----------------+-------------------+---------+
| R01     | G01, G04         | hurting AND fever   | NULL               | NULL            | P01               |     0.6 |
| R02     | G01, G02         | hurting AND swollen | NULL               | NULL            | P02               |     0.8 |
| R03     | G05              | overload            | NULL               | NULL            | P01               |     0.5 |
| R04     | G03              | red                 | P02                | trauma          | P03               |     0.8 |
| R05     | G06              | move                | P02                | trauma          | P04               |       1 |
+---------+------------------+---------------------+--------------------+-----------------+-------------------+---------+
 */