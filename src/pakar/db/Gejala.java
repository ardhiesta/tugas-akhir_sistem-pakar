package pakar.db;

import java.io.Serializable;

public class Gejala implements Serializable {
	private static final long serialVersionUID = 1L;
	private String idGejala;
	private String deskripsi;
	private double cfValue;
	public String getIdGejala() {
		return idGejala;
	}
	public void setIdGejala(String idGejala) {
		this.idGejala = idGejala;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public double getCfValue() {
		return cfValue;
	}
	public void setCfValue(double cfValue) {
		this.cfValue = cfValue;
	}
}
