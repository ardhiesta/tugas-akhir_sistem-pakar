package pakar.db;

import java.io.Serializable;

public class Penyakit implements Serializable {
	private static final long serialVersionUID = 1L;
	private String idPenyakit;
	private String deskripsi;
	private String definisi;
	private double cfValue;
	public String getDefinisi() {
		return definisi;
	}
	public void setDefinisi(String definisi) {
		this.definisi = definisi;
	}
	public double getCfValue() {
		return cfValue;
	}
	public void setCfValue(double cfValue) {
		this.cfValue = cfValue;
	}
	public String getIdPenyakit() {
		return idPenyakit;
	}
	public void setIdPenyakit(String idPenyakit) {
		this.idPenyakit = idPenyakit;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
}
