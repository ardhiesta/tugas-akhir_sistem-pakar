package pakar.db;

import java.io.Serializable;
import java.util.ArrayList;

public class Rule implements Serializable {
	private static final long serialVersionUID = 1L;
	private String idRule;
	private ArrayList<Gejala> arrGejala;
	private ArrayList<Penyakit> arrPenyakit;
	private Penyakit penyakit;
	private double cfValue;
	public Penyakit getPenyakit() {
		return penyakit;
	}
	public void setPenyakit(Penyakit penyakit) {
		this.penyakit = penyakit;
	}
	public String getIdRule() {
		return idRule;
	}
	public void setIdRule(String idRule) {
		this.idRule = idRule;
	}
	public ArrayList<Gejala> getArrGejala() {
		return arrGejala;
	}
	public void setArrGejala(ArrayList<Gejala> arrGejala) {
		this.arrGejala = arrGejala;
	}
	public ArrayList<Penyakit> getArrPenyakit() {
		return arrPenyakit;
	}
	public void setArrPenyakit(ArrayList<Penyakit> arrPenyakit) {
		this.arrPenyakit = arrPenyakit;
	}
	public double getCfValue() {
		return cfValue;
	}
	public void setCfValue(double cfValue) {
		this.cfValue = cfValue;
	}
}
