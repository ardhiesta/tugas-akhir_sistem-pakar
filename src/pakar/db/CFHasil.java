package pakar.db;

import java.io.Serializable;

public class CFHasil implements Serializable/*, Comparable*/ {
	private static final long serialVersionUID = 1L;
	private String idRule;
	private String idPenyakit;
//	private String descPenyakit;
	private double cfValue;
	
//	public String getDescPenyakit() {
//		return descPenyakit;
//	}
//	public void setDescPenyakit(String descPenyakit) {
//		this.descPenyakit = descPenyakit;
//	}
	public String getIdRule() {
		return idRule;
	}
	public void setIdRule(String idRule) {
		this.idRule = idRule;
	}
	public String getIdPenyakit() {
		return idPenyakit;
	}
	public void setIdPenyakit(String idPenyakit) {
		this.idPenyakit = idPenyakit;
	}
	public double getCfValue() {
		return cfValue;
	}
	public void setCfValue(double cfValue) {
		this.cfValue = cfValue;
	}
//	@Override
//	public int compareTo(Object cfHasil) {
//		// TODO Auto-generated method stub
//		double compareCF = ((CFHasil)cfHasil).getCfValue();
//		return (int) (this.cfValue - compareCF);
//	}
}
